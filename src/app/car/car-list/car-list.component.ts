import { Component, OnInit } from '@angular/core';
import { Car } from './model';
import { CarsService } from 'src/app/shared/cars.service';

@Component({
  selector: 'car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.scss']
})
export class CarListComponent implements OnInit {

  cars: Car[];

  constructor(private carsService: CarsService) { }

  ngOnInit() {
    this.cars = this.carsService.getCarList();
  }

  deleteCar(car: Car) {
    this.carsService.deleteCar(car);
  }
}
