export interface Car {
    mark: string;
    model: string,
    year: number
}