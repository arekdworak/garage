import { Component, OnInit } from '@angular/core';
import { Car } from '../car-list/model';
import { NgForm } from '@angular/forms';
import { CarsService } from 'src/app/shared/cars.service';

@Component({
  selector: 'app-add-car',
  templateUrl: './add-car.component.html',
  styleUrls: ['./add-car.component.scss']
})
export class AddCarComponent implements OnInit {

  years: number[] = [];

  newCar: Car = {
    mark: "",
    model: "",
    year: 0
  }

  constructor(private carsService: CarsService) { }

  ngOnInit() {
    for (let year = 1994; year <= new Date().getFullYear(); year++) {
      this.years.push(year);
    }
    this.newCar.year = this.years[0];
  }

  addCar(form: NgForm) {
    this.carsService.addNewCar(this.newCar);
    form.reset();
  }

}
