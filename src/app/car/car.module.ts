import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarListComponent } from './car-list/car-list.component';
import { RouterModule, Routes } from '@angular/router';
import { AddCarComponent } from './add-car/add-car.component';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [
  { path: 'cars', component: CarListComponent },
  { path: 'cars/add', component: AddCarComponent },
];

@NgModule({
  declarations: [
    CarListComponent,
    AddCarComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  exports: [
    CarListComponent,
    AddCarComponent
  ]
})
export class CarModule { }
