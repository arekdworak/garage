import { Injectable } from '@angular/core';
import { Car } from '../car/car-list/model';

const KEY = "garage-app-cars"

@Injectable({
  providedIn: 'root'
})
export class CarsService {


  constructor() { }

  getCarList(): Car[] {
    return JSON.parse(localStorage.getItem(KEY)) || [];
  }

  addNewCar(car: Car) {
    let currentCars = this.getCarList();
    currentCars.push(car);
    localStorage.setItem(KEY, JSON.stringify(currentCars));
  }

  deleteCar(car: Car) {
    let currentCars = this.getCarList();
    let newCars = currentCars.splice(currentCars.indexOf(car), 1);
    localStorage.setItem(KEY, JSON.stringify(currentCars));
  }
}
